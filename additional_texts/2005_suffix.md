## Дополнительные ссылки

Результаты:

- [ЧГК (потуровые)](http://studchr.chgk.info/content/view/28/6/)
- [Брейн-ринг: результаты предварительного группового этапа](http://studchr.chgk.info/content/view/29/6/)
- [Брейн-ринг: результаты основного группового этапа](http://studchr.chgk.info/content/view/30/6/)
- [Брейн-ринг: результаты плэй-офф](http://studchr.chgk.info/content/view/31/6/)

Вопросы: [ЧГК](http://db.chgk.info/tour/ruch05st), [брейн-ринг](http://db.chgk.info/tour/rubr05st).

[Всё остальное на сайте Летописи](http://letopis.chgk.info/200504Arkhangelsk.html).