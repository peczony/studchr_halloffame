## Дополнительные ссылки

Результаты:

- [ЧГК (потуровые)](http://studchr.chgk.info/content/view/53/5/)
- [Брейн-ринг](http://letopis.chgk.info/200604Tyumen-resBrain.html)

Вопросы: [ЧГК](http://db.chgk.info/tour/ruch06st), [брейн-ринг](http://db.chgk.info/tour/rubr06st).

[Всё остальное на сайте Летописи](http://letopis.chgk.info/200604Tyumen.html).