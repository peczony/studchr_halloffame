## Дополнительные ссылки

- [Брейн-ринг: индивидуальная результативность](http://studchr.chgk.info/content/view/63/3/)
- [Брейн-ринг: результаты боёв площадки 1](http://studchr.chgk.info/content/view/59/3/)
- [Брейн-ринг: результаты боёв площадки 2](http://studchr.chgk.info/content/view/62/3/)
- [Брейн-ринг: результаты боёв площадки 3](http://studchr.chgk.info/content/view/58/3/)
- [Брейн-ринг: результаты матчей финальной группы](http://studchr.chgk.info/content/view/57/3/)

[Вопросы](http://db.chgk.info/tour/ruch08st), [фото](https://chgk-photo.livejournal.com/7109.html), [ЖЖ](https://chgk-student.livejournal.com/?tag=%D1%81%D1%82%D1%83%D0%B4%D1%87%D1%80-2008), [всё остальное на сайте Летописи](http://letopis.chgk.info/200804Kazan.html).