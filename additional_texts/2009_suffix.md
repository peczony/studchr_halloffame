## Дополнительные ссылки

Результаты:

- [ЧГК (потуровые)](http://studchr.chgk.info/content/view/54/96/)
- [Брейн-ринг](http://studchr.chgk.info/content/view/56/96/)
- [Брейн-ринг: индивидуальная результативность](http://studchr.chgk.info/content/view/65/96/)
- [Брейн-ринг: полная статистика турнира](http://studchr.chgk.info/content/view/66/96/)
- [Брейн-ринг: протоколы полуфинальных боёв](http://studchr.chgk.info/content/view/60/96/)
- [Брейн-ринг: протокол матча за 3-е место](http://studchr.chgk.info/content/view/61/96/)
- [Брейн-ринг: протокол финальных боёв](http://studchr.chgk.info/content/view/55/96/)


[Вопросы](http://db.chgk.info/tour/ruch09st), [всё остальное на сайте Летописи](http://letopis.chgk.info/200904NizhnyNovgorod.html).