## Дополнительные ссылки

[Потуровые результаты ЧГК](http://studchr.chgk.info/content/view/96/97/).

Впечатления участников:

- [статья в газете «За науку»](https://mipt.ru/za-nauku/hardcopies/2010/f_4jdwaw/a_4jdyph.php).
- [Отчёт Михаила Пахнина](https://general-kollaps.livejournal.com/49751.html) (ЖЖ)


[Вопросы](http://db.chgk.info/tour/ruch10st), [фото](https://vk.com/album-868816_108423915), [всё остальное на сайте Летописи](http://letopis.chgk.info/201004Togliatti.html).