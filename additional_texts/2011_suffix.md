## Дополнительные ссылки

Гугл-таблицы:

- [Спортивная «Своя игра»](https://docs.google.com/spreadsheets/u/0/d/1Z-hiePcc1hKibvPlZ-gX0lOtLHgi0OEB4wFHSARUAfw/pub?hl=ru&hl=ru&hl=ru&output=html)
- [Брейн-ринг](https://spreadsheets.google.com/spreadsheet/pub?hl=ru&hl=ru&key=0AoH63Yvgr3jxdG9yZmNSemdiZjZyZXdyWVRmM1k4N3c&output=html), [индивидуальная результативность](https://spreadsheets.google.com/spreadsheet/pub?hl=ru&hl=ru&key=0AoH63Yvgr3jxdDI2bkJ6bnctbFRZYTZ1X1pib1ZYbUE&single=true&gid=0&output=html)


[Вопросы](https://db.chgk.info/tour/ruch12st), [фото](https://vk.com/album-463723_132792141), [трансляция чемпионата в твиттере](https://twitter.com/StudChemp2011), [отчёт оргкомитета](http://studchr.chgk.info/content/blogsection/14/100/), [всё остальное на сайте Летописи](http://letopis.chgk.info/201104Saransk.html).

MVP чемпионата стал [Андрей Островский](https://rating.chgk.info/player/23737).