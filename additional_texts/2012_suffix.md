## Дополнительные ссылки

Гугл-таблицы:

- [Брейн-ринг](https://docs.google.com/spreadsheets/u/0/d/1CmZSR6Z2gwGwXA7AhzqO4ymGyR0bnI70s_BGeofCJkI/pub?output=html)
- [Спортивная «Своя игра»](https://docs.google.com/spreadsheet/pub?key=0AoH63Yvgr3jxdHg2VDhpTk1wcUlYRzBXcFpGNEZFeHc&output=html)


Вопросы: [ЧГК](https://db.chgk.info/tour/ruch12st)

[Отчёт оргкомитета](http://studchr.chgk.info/content/view/162/101/), [всё остальное на сайте Летописи](http://letopis.chgk.info/201204NNovgorod.html).

MVP чемпионата стал [Алексей Шередега](https://rating.chgk.info/player/36035).