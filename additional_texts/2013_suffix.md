## Дополнительные ссылки

Гугл-таблицы:

- [Брейн-ринг](https://docs.google.com/spreadsheets/d/1Tb0uJeSFI2z9oz7iABc-qtUkLbavZ9fnjB8cq1FL5lo/edit?usp=sharing)
- [Спортивная «Своя игра»](https://docs.google.com/spreadsheets/d/18DjoFNiP6KJS5fCDO-XM01A-ExiJckQJUsk01j0lJQE/edit?usp=sharing)

Вопросы: [ЧГК](http://db.chgk.info/cgi-bin/db.cgi?tour=ruch13st).

[Паблик Вконтакте](https://vk.com/stud_chgk), [фото](https://vk.com/album-51746581_173269809), [всё остальное на сайте Летописи](http://letopis.chgk.info/201304Kursk.html).