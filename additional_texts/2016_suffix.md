## Дополнительные ссылки

Таблицы:

- [ЧГК](https://web.archive.org/web/20170626043623/http://studchr16.intring.ru/)
- [Брейн-ринг](https://docs.google.com/spreadsheets/d/1vUiVWGKLE-go5pa7CwVehCVQUzNgLchXl_jW2nu0Zb4/edit#gid=894224477)
- [Спортивная «Своя игра»](https://docs.google.com/spreadsheets/d/1uvYgbhchbM_lPRvREmkgScKMnelHcQ5X8xbGpQx2-So/edit#gid=621664105)
- [Эрудит-квартет](https://docs.google.com/spreadsheets/d/1CqXRcUDbCGK1UAEilUt1kblZwVSBXPbvOY7IedX4T6c/edit#gid=182347289)
- [MVP](https://docs.google.com/spreadsheets/d/1Ctm8GZdQP-tVLEZRTfchQe0dVjEswQ1130vZFxEZiZQ/edit?gid=0#gid=0)

Вопросы: [ЧГК](https://db.chgk.info/tour/ruch16st).

[Паблик Вконтакте](https://vk.com/studchr2016), [фото](https://vk.com/albums-118450630)