## Дополнительные ссылки

Гугл-таблицы:

- [ЧГК](https://docs.google.com/spreadsheets/d/1hg9KLnUNaYXVncE98eR9xY5SvUJU5WHPgEwtPca1r48/edit?gid=1127379251#gid=1127379251)
- [Брейн-ринг](https://docs.google.com/spreadsheets/d/1bTkL3xqvO0GAe9pPlGjrDjjJQMELIGcNmjjd-7cj4jE/edit)
- [Спортивная «Своя игра»](https://docs.google.com/spreadsheets/d/1O19cn071J96WINLuMrUjpcDKd2gbrRzG80sZAmlZEMc/edit)
- [Эрудит-квартет](https://docs.google.com/spreadsheets/d/14c3E-5GNV7wDXqifDmvtDWHzxc5Vg0gmy5whTUJL24I/edit)
- [Индивидуальная статистика](https://docs.google.com/spreadsheets/d/1wUDjEcau3gfakEwhJ2X06GaCrRbRd4oRLKQqq1PjH3A/edit?gid=0#gid=0)

Вопросы: [ЧГК](https://db.chgk.info/tour/ruch17st)

[Паблик Вконтакте](https://vk.com/studchr2017), [фото](https://vk.com/albums-142618415).