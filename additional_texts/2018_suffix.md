## Дополнительные ссылки

Результаты:

- [ЧГК](https://rating.chgk.info/tournament/4585),\
- [Брейн-ринг](https://docs.google.com/spreadsheets/d/1XuWbEIRxvGPaa3cr-Jihm9J6dKKTY6XIM_CRQSIro5o/edit#gid=894224477),\
- [Эрудит-квартет](https://docs.google.com/spreadsheets/d/1so8qfDCQ32j8DtefCs6LVmKRm8YXk393JG-e4VC7xt0/edit?ts=5acc5c25#gid=0),\
- [Своя игра](https://docs.google.com/spreadsheets/d/1IC-apL--tNZLLECmTkecYPNBc30XppXAjFg1K_UnI8A/edit#gid=1711054203).
- [MVP](https://docs.google.com/spreadsheets/d/1PgXm9lXwJzax3XtTWtCUxjQEL_-QPjC2uyw4SaWBG4o/edit?gid=0#gid=0).

Вопросы: [ЧГК](https://db.chgk.info/tour/ruch18st_u).

Иная информация:

- [Паблик Вконтакте](https://vk.com/studchr2018),
— [«Постпасхалочка. Почему Христос воскрес, а «ЗБЭР» — нет?»](https://riddler.li/2018/04/12/postpaskhalochka-pochiemu-khristos-voskries-a-zber-niet) *Лонгрид Юрия Разумова в преддверии чемпионата.*
- [подробный рассказ, как это было](https://vk.com/@studchr2018-kak-eto-bylo).