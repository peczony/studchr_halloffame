## Дополнительные ссылки

Гугл-таблицы:

- [ЧГК](https://docs.google.com/spreadsheets/d/1h-Y7UpLy0Ruuv7iO6MLMOT28ZQH9hReDM1tHRNyFg4w/edit?gid=689914267#gid=689914267)
- [Брейн-ринг](https://docs.google.com/spreadsheets/d/1ePYkzEZlxhrURsy6NX_s1XfKDoND6PTwPiwNDMiQRoA/edit#gid=1535077912)
- Спортивная «Своя игра» — [отбор и ТПШ](https://docs.google.com/spreadsheets/d/1tvLOn4l4mczERE9l5VhYd8z0PQUwF8IMD58RyLvRPhs/edit#gid=0), [основной турнир](https://docs.google.com/spreadsheets/d/1ACr7GlGTSom-D43cZdwpPOk3NIDZYV6Kquf0yhTlGgs/edit#gid=59489282)
- [Эрудит-квартет](https://docs.google.com/spreadsheets/d/1KE6XrdF9-WjyxlGMKIZRjro9C29RV4aiKh0HRKy_wiI/edit)
- [MVP](https://docs.google.com/spreadsheets/d/10w_lseA-rGjlCEMm929RGjsMgApcC0m7XJrK-h-kTkU/edit?gid=1389153033#gid=1389153033)

Вопросы: [ЧГК](https://db.chgk.info/tour/ruch19st_u)

[Паблик Вконтакте](https://vk.com/studchr2019), [фото](https://vk.com/albums-178500586), [видео](https://vk.com/studchr2019?w=page-178500586_56483653)