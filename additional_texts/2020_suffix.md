## Дополнительные ссылки

### Гугл-таблицы

- [онлайн-загадки](https://docs.google.com/spreadsheets/d/1vnfbJAAAyog9vOKGBaxfkTIkCYUE6qhHOrLT0nEk2y0/edit#gid=1502658804),
- [онлайн-Эрудит-квартет](https://docs.google.com/spreadsheets/d/1fgr5bvCjzp_tOUx4_8QNfRLLsZEusvcNQ_P2ErSq3xs/edit#gid=0)
- [MVP](https://docs.google.com/spreadsheets/d/1t51N5zOQCKhDO0R3x3jb-26ag_IltaSVEpImwAyrKEY/edit?gid=0#gid=0)

### Вопросы

- [загадки](https://db.chgk.info/tour/ruchon20st_u)

### [Паблик Вконтакте](https://vk.com/onlinestudchr2020), [фото](https://vk.com/albums-197815992), [видео](https://www.youtube.com/watch?v=nCcW_nn-T8Y)