## Дополнительные ссылки

### Гугл-таблицы

- [основная дисциплина](https://docs.google.com/spreadsheets/d/1TpvNTcd63E_wXDC5PlfrTsxorRNsP9leK2FxI1I3Hw8/edit?usp=drive_web&ouid=115266771265155782143),
- [командная игра на скорость без фальстартов](https://docs.google.com/spreadsheets/d/1w6hbksYQ46loLh_j3xmJY3Y_M196HC1NytoSs85nijw/edit#gid=1535077912),
- [эрудит-квартет](https://docs.google.com/spreadsheets/d/1i35qw0gHi3IiMcYgcmx7ugSvWXwIrsUDUl_WqdfVBUs/edit#gid=0),
- [спортивная Своя игра](https://docs.google.com/spreadsheets/d/1U8fQlvFYESuZ4kvU-7GyVTvyjYiVUmPQ4dRLC8yh6xw/edit#gid=59489282),
- [MVP](https://docs.google.com/spreadsheets/d/1ie0YfetGKoYNbgbJjUsjxjuJubpgQUUAgRNlwZb3coo/edit#gid=0) ([формула расчёта](https://docs.google.com/document/d/1Og0AtpbEAOd_9tFx3RoM01riY-HbZfMsSAc9b9qnuK4/edit#heading=h.okp9rdtkxrnj)).


### Вопросы

- [основная дисциплина](https://db.chgk.info/tour/ruch20st_u),
- [КИнСбФ](https://db.chgk.info/tour/rukisbf21st_u).

### Видео

- [финал и бой за 3-е место командной игры на скорость без фальстартов](https://vk.com/video-204038983_456239018),
- [финал эрудит-квартета](https://vk.com/video-204038983_456239017),
- [финал спортивной «Своей игры»](https://www.youtube.com/watch?v=kwk4Ibjme5o&t=1s).

### Интервью

- [«Надеемся остаться единственными в истории победителями онлайн-СтудЧРа»](https://vk.com/@studchr2021-za-sebya-i-za-gleba-romanenko). *«Флуд» a.k.a. «За себя и за Глеба Романенко!» (Санкт-Петербург) — об обновлении состава, разовых названиях и отношении к переигровкам,*
- [«В наше время особо не приходится выбирать, какие синхроны играть»](https://vk.com/@studchr2021-kudryavchik-i-vahrivka)[.](https://vk.com/@studchr2021-kudryavchik-i-vahrivka) *«Кудрявчик и Вахривка» (Иркутск) — о том, зачем играть всё, что движется, как почувствовать себя на выезде в собственной квартире и о звезде «Вышкафеста» -- коте Костике,*
- [«В планах — ворваться и развалить этот ваш СтудЧР»](https://vk.com/@studchr2021-antineuvazhenie). *«Антинеуважение» (Москва) — о впечатлениях от перехода в студенческую категорию, изменениях в отношениях с тренером и турнирных целях на чемпионат*,
- [«Не хотелось бы в новую эпоху терять то, что приносит столько положительных эмоций»](https://vk.com/@studchr2021-city-of-tatars). *«City of tatars» (Казань) — о проблемах региональных команд, плюсах и минусах онлайна и об ожиданиях от домашнего СтудЧРа*,

### [Паблик Вконтакте](https://vk.com/studchr2021), [фото](https://vk.com/albums-204038983), [статья о связанных с проведением трудностях](https://teletype.in/@studchr/igratv).