## Дополнительные ссылки

### Гугл-таблицы

- [основная дисциплина](https://docs.google.com/spreadsheets/d/1VMl5jitrtOwxKEFdHaI7YJZWfyYUI-ZK8ORSNzFa7bM/edit#gid=710076290),
- [командная игра на скорость без фальстартов](https://docs.google.com/spreadsheets/d/10Yu5o0cKnVhkPydCctR4_RPdY3oGaT6AVIrcOPyFROc/edit?usp=sharing),
- [эрудит-квартет](https://docs.google.com/spreadsheets/d/1OhXPXjfoqlJL7qElLwHI6wbgjFg9g9pkSbWAG6lS6MQ/edit#gid=0),
- [спортивная Своя игра](https://docs.google.com/spreadsheets/d/1g-4k3KmzpE4INTOHa-4JtTjQwNJtl3GmioQ-E0BTFGM/edit),
- [Турнир последнего шанса по ССИ](https://docs.google.com/spreadsheets/d/1O_l7roJTZbrucP6p5pa3h7A66p3aQaSPLRiaO6naZNA/edit#gid=2109386310),
- [MVP](https://docs.google.com/spreadsheets/d/1i069E_vPBLvxGBf4P5KbsdHX57Eiqvuimf2uhW4EoqM/edit) ([формула подсчёта](https://docs.google.com/document/d/15B3QF-G-dKzc1oZwXRDBS3ndWI17YNZDxk412gW7lME/edit#heading=h.okp9rdtkxrnj)).


### Вопросы

- [основная дисциплина](https://db.chgk.info/tour/ruch22st_u).
- [командная игра на скорость без фальстартов](https://db.chgk.info/tour/rukisbf22st_u)

### Видео

- [финал спортивной «Своей игры»](https://vk.com/video-99683830_456239035),
- [финал эрудит-квартета](https://vk.com/video-99683830_456239036),
- [финал командной игры на скорость без фальстартов](https://www.youtube.com/watch?v=-Z7GCTjnx38).

### [Паблик вконтакте](https://vk.com/studchr2022)