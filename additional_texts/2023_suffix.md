## Дополнительные ссылки

### Гугл-таблицы

- [основная дисциплина](https://docs.google.com/spreadsheets/d/1TJChhZrYqqfo48u9fctk071sH2YMOfWSjsdXz3DwprU/edit#gid=689914267)
- [командная игра на скорость без фальстартов](https://docs.google.com/spreadsheets/d/1gY6od8Sp5YM9WugShlPMgCJYuBzkrLUR-wSPCfGDb4g/edit),
- [эрудит-квартет](https://docs.google.com/spreadsheets/d/19K7N9gzL_C_jD-QAqeeWRnc4ylLxG78MHt5KiJJSkpU/edit),
- [спортивная Своя игра](https://docs.google.com/spreadsheets/d/14_Pc3kvtoLhXvByFpQZnYH1VpVt-zHUrvfk3VCEednY/edit),
- [Турнир последнего шанса по ССИ](https://docs.google.com/spreadsheets/d/1mPIODHdIlwH-wS_CG_8PK6bDChzHxNv39jj8cQ80K14/edit?usp=drive_web&ouid=115266771265155782143),
- [MVP](https://docs.google.com/spreadsheets/d/1K0WkK2ws8N515vqD37FX5H3iXdtqLUdMS-ILmAd_FRo/edit#gid=0) ([формула подсчёта](https://docs.google.com/document/d/1EwYdPa_T91z8P6Zg8CGVZTtjGJYNYNJNgRm1UPG7S8o/edit#heading=h.okp9rdtkxrnj)).

### Вопросы

- [основная дисциплина](https://gotquestions.online/pack/5543)
- [командная игра на скорость без фальстартов](https://gotquestions.online/pack/5810)

### Фото

- Пятница (19 мая): [ССИ](https://vk.com/album-219993759_292744679).
- Суббота (20 мая): [ОД и КИнСбФ](https://vk.com/album-219993759_293437380).
- Воскресенье (21 мая): [ОД](https://vk.com/album-219993759_293480754), [финалы](https://vk.com/album-219993759_293480991), [награждение](https://vk.com/album-219993759_293481192).

### [Паблик вконтакте](https://vk.com/studchr2023)