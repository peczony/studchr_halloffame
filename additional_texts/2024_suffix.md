## Дополнительные ссылки

### Гугл-таблицы

- [Основная дисциплина](https://docs.google.com/spreadsheets/d/1DAmWrHegJEzK27WQXExJofTGz76mW62qDGokCpewE8c/edit#gid=1039476412)
- [Командная игра на скорость без фальстартов](https://docs.google.com/spreadsheets/d/1MwToR42cAdvMTYEfalJIkuMvCTWJKBQIOJCHHj1lyps/edit#gid=1807640939)
- [Спортивная «Своя игра»](https://docs.google.com/spreadsheets/d/1H-Cn3jxrvfmaNXcc-u9WXpm-mZhN9yNofQmm5qzLYxE/edit#gid=125287118)
- [Эрудит-квартет](https://docs.google.com/spreadsheets/d/1SUSGnK51GpIvIZi_4JJa_Tq_PzYyXA0M2uIgvsdz36M/edit#gid=1010437548)
- [MVP](https://docs.google.com/spreadsheets/d/1R4qPmAK-L6G_0MFbRwyA_J6M_wcDGVZS3GtIZ1u9Btc/edit#gid=0)
- [Турнир последнего шанса по ССИ](https://docs.google.com/spreadsheets/d/1wmHQo_lW5rre7Wh--P2Ix3Bp-BMsHSMwZq7azIbWk7s/edit#gid=375476770)

### Вопросы

будут опубликованы позже

### Фото

будут опубликованы позже

### [Телеграм-канал](https://t.me/studchr2024)