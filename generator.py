#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, unicode_literals

import codecs
import os
from collections import Counter, defaultdict

import yaml

from stats_from_results import player_to_tuple, sorting, teamsorting
from preemstvennost import get_recaps_set, has_preemstvennost


STATIC = """\
<script src="/responsive-tables.js"></script>
"""


def read_if_exists(path):
    if os.path.isfile(path):
        with open(path, "r") as f:
            cnt = f.read()
        return cnt


def generate_results(champ, template):
    content = ""
    year = int(champ["_meta"]["date_start"].split("-")[0])
    datestart = champ["_meta"]["date_start"].split(" ")[0]
    dateend = champ["_meta"]["date_end"].split(" ")[0]
    prefix = read_if_exists(f"additional_texts/{year}_prefix.md")
    suffix = read_if_exists(f"additional_texts/{year}_suffix.md")
    mentioned_teams = set()
    content += "{}, {}{}\n\n".format(
        champ["_meta"]["town"],
        "{}–{}".format(datestart, dateend)
        if datestart != dateend
        else "{}".format(datestart),
        " ([на турнирном сайте](http://rating.chgk.info/tournament/{}))".format(
            champ["_meta"]["idtournament"]
        )
        if "idtournament" in champ["_meta"]
        else "",
    )
    if prefix:
        content += prefix
    if "chgk" in champ:
        if year < 2020:
            name = "Что? Где? Когда?"
        elif year == 2020:
            name = "Игра в загадки"
        else:
            name = "Основная дисциплина"
        content += f"## {name}\n\n"
        prizes, mentioned_teams = format_prizes(champ["chgk"], mentioned_teams)
        content += prizes
    if "brain" in champ:
        if year < 2020:
            name = "Брейн-ринг"
        else:
            name = "Командная игра на скорость без фальстартов"
        content += f"## {name}\n\n"
        prizes, mentioned_teams = format_prizes(champ["brain"], mentioned_teams)
        content += prizes
    if "ek" in champ:
        content += "## Эрудит-квартет\n\n"
        prizes, mentioned_teams = format_prizes(champ["ek"], mentioned_teams)
        content += prizes
    if "si" in champ:
        content += "## Своя игра\n\n"
        for prize in sorted(champ["si"]):
            content += "{}-е место: {}  \n".format(
                prize, format_player(champ["si"][prize])
            )
    if suffix:
        content += suffix
    final = template.format(
        date=champ["_meta"]["date_start"].split(" ")[0],
        description="СтудЧР-{}".format(year),
        slug=year,
        title="СтудЧР-{}".format(year) if year != 2020 else "Онлайн-СтудЧР",
        content=content,
    )
    with codecs.open(f"content/results/{year}.md", "w", "utf8") as f:
        f.write(final)


def get_teams_from_year(results, year):
    res = results[year]
    result = []
    team_ids = set()
    for d in ("chgk", "brain", "ek"):
        disc = res.get(d)
        if not disc:
            continue
        for place in disc:
            team = disc[place]
            if team["id"] not in team_ids:
                team_ids.add(team["id"])
                result.append(team)
    return result


def generate_preemstvennost_map(results):
    years = sorted(list(results.keys()))
    output = {}
    cur_id = -1
    for i, year in enumerate(years):
        if year == 2001:
            continue
        teams_cur_year = get_teams_from_year(results, year)
        teams_prev_years = [(y, get_teams_from_year(results, y)) for y in years[i - 7 : i]][::-1]
        for team in teams_cur_year:
            cur_team_ids = get_recaps_set(team)
            for _, teams in teams_prev_years:
                for second_team in teams:
                    if second_team["id"] == team["id"]:
                        continue
                    if has_preemstvennost(get_recaps_set(second_team), cur_team_ids):
                        new_id = team["id"]
                        old_id = second_team["id"]
                        if old_id in output:
                            output[new_id] = output[old_id]
                        else:
                            output[old_id] = cur_id
                            output[new_id] = cur_id
                            cur_id -= 1
    return output


def format_prizes(prizes, mentioned_teams):
    content = ""
    for prize in sorted(prizes):
        content += "{}-е место: {} ({})  \n\n".format(
            int(prize), prizes[prize]["name"], prizes[prize]["town"]
        )
        if prizes[prize]["id"] not in mentioned_teams:
            for player in prizes[prize]["recaps"]:
                content += "  - {}  \n".format(format_player(player))
            mentioned_teams.add(prizes[prize]["id"])
        content += "\n\n"
    return content, mentioned_teams


def format_player(player):
    if isinstance(player, tuple):
        player = {"id": player[0], "name": player[1]}
    return "[{}](http://rating.chgk.info/player/{})".format(
        player["name"], player["id"]
    )


def html_format_player(player):
    if isinstance(player, tuple):
        player = {"id": player[0], "name": player[1]}
    return '<a href="http://rating.chgk.info/player/{}">{}</a>'.format(
        player["id"], player["name"]
    )


def html_tabulate(*args, **kwargs):
    th = kwargs.get("th")
    fra = kwargs.get("fra")
    if not fra:
        return "<tr>" + "".join(map(make_th if th else make_td, args)) + "</tr>"
    else:
        func = make_th if th else make_td
        first = [func(args[0], class_="right_aligned")]
        args = list(map(func, args[1:]))
        return "<tr>" + "".join(first + args) + "</tr>"


def make_td(x, class_=False):
    if not class_:
        return "<td>{}</td>".format(x)
    return '<td class="{}">{}</td>'.format(class_, x)


def make_th(x, class_=False):
    if not class_:
        return "<th>{}</th>".format(x)
    return '<th class="{}">{}</th>'.format(class_, x)


def generate_halloffame(results, template, content):
    players = defaultdict(lambda: defaultdict(lambda: Counter()))
    teams = defaultdict(lambda: defaultdict(lambda: Counter()))
    teamnames = defaultdict(lambda: set())
    for year in sorted(results):
        for sport in ("brain", "ek", "si", "chgk"):
            if sport in results[year]:
                for place in results[year][sport]:
                    team = results[year][sport][place]
                    if "recaps" in team:
                        for player in team["recaps"]:
                            players[player_to_tuple(player)][sport][place] += 1
                        teams[team["id"]][sport][place] += 1
                        teamnames[team["id"]].add(team["name"])
                    else:
                        players[player_to_tuple(team)][sport][place] += 1
    content += '<div class="wide-wrapper"><table>\n<thead>\n'
    content += html_tabulate(
        "Игрок",
        "Σ1",
        "Σ2",
        "Σ3",
        "ΣΣ",
        "Ч1",
        "Ч2",
        "Ч3",
        "Б1",
        "Б2",
        "Б3",
        "С1",
        "С2",
        "С3",
        "Э1",
        "Э2",
        "Э3  \n",
        th=True,
        fra=True,
    )
    content += "</thead>\n<tbody>\n"

    for player in sorted(players, key=lambda x: sorting(players, x), reverse=True):
        content += (
            html_tabulate(
                html_format_player(player), *sorting(players, player), fra=True
            )
            + "  \n"
        )
    content += "</tbody>"
    content += "</table>"
    content += "</div>"
    content += STATIC
    final = template.format(
        description="Зал славы",
        slug="halloffame",
        title="Зал славы",
        date="2023-09-26",
        content=content,
    )
    return final


def generate_team_link(name, id_):
    return f"""<a href="https://rating.chgk.info/teams/{id_}">{name}</a>"""


def html_format_team(team_id, preemstvennost_map, teamnames):
    if team_id < 0:
        all_team_ids = [
            x for x in preemstvennost_map if preemstvennost_map[x] == team_id
        ]
        all_team_names = []
        for ti in all_team_ids:
            team_names = teamnames[ti]
            all_team_names.extend([(n, generate_team_link(n, ti)) for n in team_names])
        return " / ".join(x[1] for x in sorted(all_team_names))
        
    else:
        srt = sorted(teamnames[team_id])
        first = generate_team_link(srt[0], team_id)
        other = srt[1:]
        if other:
            return first + " / " + " / ".join(other)
        else:
            return first


def generate_team_halloffame(results, template, content):
    preemstvennost_map = generate_preemstvennost_map(results)
    teams = defaultdict(lambda: defaultdict(lambda: Counter()))
    teamnames = defaultdict(lambda: set())
    content = content.replace("С — индивидуальная своя игра,  \n", "")
    for year in sorted(results):
        for sport in ("brain", "ek", "chgk"):
            if sport in results[year]:
                for place in results[year][sport]:
                    team = results[year][sport][place]
                    if team["id"] in preemstvennost_map:
                        team_id = preemstvennost_map[team["id"]]
                    else:
                        team_id = team["id"]
                    teams[team_id][sport][place] += 1
                    teamnames[team["id"]].add(team["name"])
    content += (
        '<div class="wide-wrapper"><table>\n<thead>\n'
    )
    content += html_tabulate(
        "Команда",
        "Σ1",
        "Σ2",
        "Σ3",
        "ΣΣ",
        "Ч1",
        "Ч2",
        "Ч3",
        "Б1",
        "Б2",
        "Б3",
        "Э1",
        "Э2",
        "Э3  \n",
        th=True,
        fra=True,
    )
    content += "</thead>\n<tbody>\n"

    for team in sorted(teams, key=lambda x: teamsorting(teams, x), reverse=True):
        content += (
            html_tabulate(
                html_format_team(team, preemstvennost_map, teamnames), *teamsorting(teams, team), fra=True
            )
            + "  \n"
        )
    content += "</tbody>"
    content += "</table>"
    content += "</div>"
    content += STATIC
    final = template.format(
        description="Зал славы (команды)",
        slug="halloffame_teams",
        title="Зал славы (команды)",
        date="2023-09-26",
        content=content,
    )
    return final


def main():
    data = yaml.safe_load(open("results.yaml"))

    with codecs.open("template.md", "r") as f:
        template = f.read()
    with codecs.open("content.txt", "r") as f:
        content = f.read()

    if not os.path.isdir("content/results"):
        os.makedirs("content/results")
    for champ in data:
        generate_results(data[champ], template)

    with codecs.open("content/results/halloffame.md", "w") as f:
        f.write(generate_halloffame(data, template, content))

    with codecs.open("content/results/halloffame_teams.md", "w") as f:
        f.write(generate_team_halloffame(data, template, content))


if __name__ == "__main__":
    main()
