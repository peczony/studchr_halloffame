# coding: utf-8
from pathlib import Path

import yaml

config = yaml.safe_load(Path("results.yaml").read_text())
years = sorted(list(config.keys()))
DISCIPLINES = ["brain", "chgk", "ek"]


def get_recaps_set(team):
    return {x["id"] for x in team["recaps"]}


def get_teams_from_year(year):
    res = config[year]
    result = []
    team_ids = set()
    for d in DISCIPLINES:
        disc = res.get(d)
        if not disc:
            continue
        for place in disc:
            team = disc[place]
            if team["id"] not in team_ids:
                team_ids.add(team["id"])
                result.append(team)
    return result


def has_preemstvennost(set1, set2):
    intersection = set1 & set2
    return len(intersection) > 0.5 * min(len(set1), len(set2))


def main():
    for i, year in enumerate(years):
        if year == 2001:
            continue
        teams_cur_year = get_teams_from_year(year)
        teams_prev_years = [(y, get_teams_from_year(y)) for y in years[i - 7 : i]][::-1]
        for team in teams_cur_year:
            cur_team_ids = get_recaps_set(team)
            for year_, teams in teams_prev_years:
                for second_team in teams:
                    if second_team["id"] == team["id"]:
                        continue
                    if has_preemstvennost(get_recaps_set(second_team), cur_team_ids):
                        print(
                            f"{year} {team['id']} {team['name']} <-> {year_} {second_team['id']} {second_team['name']}"
                        )


if __name__ == "__main__":
    main()
