#!/usr/bin/env python
#! -*- coding: utf-8 -*-
import argparse

import pyaml
import requests

NAME_OVERRIDES = {
    20461: "Анастасия Митько (Медведева)",
    41203: "Виктор Танаш (Дзекановский)",
    107372: "Юлия Маврина (Аксёненко)",
}


def get_name(player):
    id_ = player["player"]["id"]
    if id_ in NAME_OVERRIDES:
        return NAME_OVERRIDES[id_]
    surname = player["player"]["surname"].strip()
    name = player["player"]["name"].strip()
    return f"{name} {surname}"


def get_results(tourn_id):
    url = f"https://api.rating.chgk.net/tournaments/{tourn_id}/results.json?includeTeamMembers=1"
    return requests.get(url).json()


def transform_res(res):
    return {
        "id": res["team"]["id"],
        "name": res["current"]["name"],
        "town": res["current"]["town"]["name"],
        "recaps": sorted(
            [
                {
                    "id": player["player"]["id"],
                    "name": get_name(player),
                }
                for player in res["teamMembers"]
            ],
            key=lambda x: x["name"],
        ),
    }


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("tourn_id")
    args = parser.parse_args()

    info = requests.get(
        f"https://api.rating.chgk.net/tournaments/{args.tourn_id}.json"
    ).json()
    year = info["dateStart"].split("-")[0]
    res = [transform_res(res) for res in get_results(args.tourn_id)]
    with open(f"{year}-{args.tourn_id}.yaml", "w") as f:
        f.write(pyaml.dumps(res))


if __name__ == "__main__":
    main()
