#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
from collections import defaultdict

import pyaml
import yaml
from tteam import get_results, transform_res


def make_id_to_name(results):
    id_to_name = defaultdict(set)
    for year in sorted(results):
        for k in sorted(results[year].keys()):
            if k == "_meta":
                continue
            for place in (1, 2, 3):
                res_dict = results[year][k][place]
                if k == "si":
                    player = res_dict
                    id_to_name[player["id"]].add(player["name"])
                else:
                    for player in res_dict["recaps"]:
                        id_to_name[player["id"]].add(player["name"])
    for k, v in list(id_to_name.items()):
        if len(v) > 1:
            print(f"multiple names for player {k}: {v}")
        id_to_name[k] = list(v)[0]
    return id_to_name


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--filename", "-f", default="results.yaml")
    parser.add_argument("--canonize", action="store_true")
    args = parser.parse_args()

    with open(args.filename, "r", encoding="utf8") as f:
        results = yaml.safe_load(f)
    id_to_name = make_id_to_name(results)
    for year in sorted(results):
        print(f"processing year {year}")
        keys = sorted(results[year].keys())
        for k in keys:
            if k == "_meta":
                tournament_id = results[year][k]["idtournament"]
                print(f"getting results from {tournament_id}")
                team_res = {
                    x["team"]["id"]: transform_res(x)
                    for x in get_results(tournament_id)
                }
                id_to_name_rating = {}
                for team_id in team_res:
                    for player in team_res[team_id]["recaps"]:
                        id_to_name_rating[player["id"]] = player["name"]
            elif k == "si":
                for place in (1, 2, 3):
                    player = results[year][k][place]
                    try:
                        new_name = id_to_name_rating.get(player["id"])
                    except KeyError:
                        print(f"[{tournament_id}] player {player['id']} {player['name']} not found in new version")
                        continue
                    if player["name"] != new_name:
                        print(
                            f"[{tournament_id}] name difference for id {player['id']}: old {player['name']}, new {new_name}"
                        )
            else:
                for place in (1, 2, 3):
                    old_res = results[year][k][place]
                    team_id = old_res["id"]
                    try:
                        new_res = team_res[team_id]
                    except KeyError:
                        print(f"team {team_id} not found in tournament {tournament_id}")
                        continue
                    old_recaps = {x["id"] for x in old_res["recaps"]}
                    new_recaps = {x["id"] for x in new_res["recaps"]}
                    new_id_to_name = {x["id"]: x["name"] for x in new_res["recaps"]}
                    old_new_diff = old_recaps - new_recaps
                    for id_ in old_new_diff:
                        print(f"[{tournament_id}] team {team_id} {old_res['name']} player {id_} {id_to_name[id_]} was present, but is now missing")
                    new_old_diff = new_recaps - old_recaps
                    for id_ in new_old_diff:
                        print(
                            f"[{tournament_id}] team {team_id} {old_res['name']} player {id_} {new_id_to_name[id_]} was missing, but is now present"
                        )
                    if old_res["name"] != new_res["name"]:
                        print(
                            f"[{tournament_id}] team name difference detected for team {team_id}: old name {old_res['name']}, new name {new_res['name']}"
                        )
                    for id_ in (old_recaps & new_recaps):
                        old_name = id_to_name[id_]
                        new_name = id_to_name_rating[id_]
                        if old_name != new_name:
                            print(f"[{tournament_id}] name difference detected for player {id_}: old name {old_name}, new name {new_name}")
                    if old_new_diff or new_old_diff or args.canonize:
                        results[year][k][place] = new_res
                        results[year][k][place]["name"] = (
                            old_res["name"]  # don't change name!
                        )

    with open(args.filename, "w", encoding="utf8") as f:
        f.write(pyaml.dumps(results))


if __name__ == "__main__":
    main()
